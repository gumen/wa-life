import { promises as fs } from 'fs'
import assert from 'assert'
import build from './build.js'

const BOARD_SIZE = 50

async function instantiate() {
  const file = await fs.readFile('./main.wasm')
  const module = await WebAssembly.compile(file)
  const instance = await WebAssembly.instantiate(module)
  return instance.exports
}

let wasm

beforeEach(async () => {
  await build('main.wat', 'main.wasm')
})

describe('main.wasm', () => {

  beforeEach(async () => {
    wasm = await instantiate()
  })

  it('hello world returns 42', () => {
    assert.equal(wasm.helloWorld(), 42)
  })

  it('offsetFromCoordinate', () => {
    //                                    f(x, y)->((x + y * BOARD_SIZE) * 4)
    assert.equal(wasm.offsetFromCoordinate( 0, 0), ( 0 + 0 * BOARD_SIZE) * 4)
    assert.equal(wasm.offsetFromCoordinate(49, 0), (49 + 0 * BOARD_SIZE) * 4)
    assert.equal(wasm.offsetFromCoordinate(10, 2), (10 + 2 * BOARD_SIZE) * 4)
  })

  it('get or set cell', () => {
    assert.equal(wasm.getCell(2, 2), 0)
    wasm.setCell(2, 2, 1)
    assert.equal(wasm.getCell(2, 2), 1)
  })

  it ('read memory directly', () => {
    const memory = new Uint32Array(wasm.memory.buffer, 0, BOARD_SIZE * BOARD_SIZE)
    wasm.setCell(2, 2, 10)
    assert.equal(memory[2 + 2 * BOARD_SIZE], 10)
    assert.equal(memory[3 + 2 * BOARD_SIZE], 0)
  })
})
