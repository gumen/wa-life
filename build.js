import { promises as fs } from 'fs'
import getWabt from 'wabt'

export default async function(inputFilePath, outputFilePath) {
  // sry Yoda
  try {

    const wabt = await getWabt()
    const file = await fs.readFile(inputFilePath, 'utf8')

    const wasmModule = wabt.parseWat(inputFilePath, file)
    const moduleBinary = wasmModule.toBinary({})
    const moduleBuffer = Buffer.from(moduleBinary.buffer)

    await fs.writeFile(outputFilePath, moduleBuffer)

  } catch (error) {
    console.error('>>>> It\'s a trap!\n', error)
  }
}
