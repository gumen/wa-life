import { promises as fs } from 'fs'
import assert from 'assert'
import build from './build.js'

const name = 'memory'
const inputFilePath = `./${name}.wat`
const outputFilePath = `./${name}.wasm`

const memory = new WebAssembly.Memory({ initial: 1 })

function getStringFromMemory(memory, offset, length) {
  const bytes = new Uint8Array(memory.buffer, offset, length)
  return new TextDecoder('utf8').decode(bytes)
}

async function instantiate() {
  const file = await fs.readFile(outputFilePath)
  const module = await WebAssembly.compile(file)
  const instance = await WebAssembly.instantiate(module, {
    imports: {
      mem: memory
    }
  })
  return instance.exports
}

let wasm

describe(name + '.wasm', async () => {

  before(async () => {
    await build(inputFilePath, outputFilePath)
  })

  beforeEach(async () => {
      wasm = await instantiate()
  })

  it('write "Hi" in memory', () => {
    assert.equal(getStringFromMemory(memory, 0, 2), 'Hi')
  })
})
