import { promises as fs } from 'fs'

const file = await fs.readFile('main.wasm')
const module = await WebAssembly.compile(file)
const instance = await WebAssembly.instantiate(module)

console.log(instance.exports.helloWorld())
