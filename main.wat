(module
  (memory 1) ;; 1 - require at leas one page (64KiB)
  (export "memory" (memory 0))

  ;; It's also possible to give memory a label
  ;; (memory $mem 1)
  ;; (export "memory" (memory $mem))
  ;;
  ;; Other way of exporting
  ;; (memory (export "memory") 1)

  (func $test_func (result i32)
    (local i32)
    (set_local 0 (i32.const 42))
    (get_local 0))

  (func (export "helloWorld") (result i32)
    (call $test_func))

  ;; f(x,y) -> ((y * 50 + x) * 4)
  (func $offsetFromCoordinate (export "offsetFromCoordinate")
    (param $x i32) (param $y i32) (result i32)
    (i32.mul
      (i32.add
        (i32.mul (get_local $y) (i32.const 50))
        (get_local $x))
      (i32.const 4)))

  (func $setCell (export "setCell")
    (param $x i32) (param $y i32) (param $value i32)
    (i32.store
      (call $offsetFromCoordinate
        (get_local $x)
        (get_local $y))
      (get_local $value)))

  (func $getCell (export "getCell")
    (param $x i32) (param $y i32) (result i32)
    (i32.load
      (call $offsetFromCoordinate
        (get_local $x)
        (get_local $y))))

)
